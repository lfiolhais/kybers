#![feature(test)]

extern crate sha3;
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use sha3::Keccak;

    #[bench]
    fn bench_sha3_256_input_32_bytes(b: &mut test::Bencher) {
        let data = vec![254u8; 32];
        b.bytes = data.len() as u64;

        b.iter(|| {
            let mut res: [u8; 32] = [0; 32];
            sha3::sha3_256(&data, &mut res);
        });
    }

    #[bench]
    fn bench_sha3_256_input_4096_bytes(b: &mut test::Bencher) {
        let data = vec![254u8; 4096];
        b.bytes = data.len() as u64;

        b.iter(|| {
            let mut res: [u8; 32] = [0; 32];
            sha3::sha3_256(&data, &mut res);
        });
    }

    #[bench]
    fn bench_shake_128_input_4096_bytes(b: &mut test::Bencher) {
        let data = vec![254u8; 4096];
        b.bytes = data.len() as u64;

        b.iter(|| {
            let mut res = [0u8; 168];
            let mut shake = Keccak::shake128();
            shake.absorb(&data);
            shake.squeeze(&mut res);
        });
    }

    #[bench]
    fn bench_shake_128_input_8192_bytes(b: &mut test::Bencher) {
        let data = vec![254u8; 8192];
        b.bytes = data.len() as u64;

        b.iter(|| {
            let mut res = [0u8; 168];
            let mut shake = Keccak::shake128();
            shake.absorb(&data);
            shake.squeeze(&mut res);
        });
    }
}
