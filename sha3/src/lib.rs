//! SHA3 Implementation

//#![no_std]

const ROUND_CONSTANTS: [u64; 24] = [
    0x0000_0000_0000_0001,
    0x0000_0000_0000_8082,
    0x8000_0000_0000_808A,
    0x8000_0000_8000_8000,
    0x0000_0000_0000_808B,
    0x0000_0000_8000_0001,
    0x8000_0000_8000_8081,
    0x8000_0000_0000_8009,
    0x0000_0000_0000_008A,
    0x0000_0000_0000_0088,
    0x0000_0000_8000_8009,
    0x0000_0000_8000_000A,
    0x0000_0000_8000_808B,
    0x8000_0000_0000_008B,
    0x8000_0000_0000_8089,
    0x8000_0000_0000_8003,
    0x8000_0000_0000_8002,
    0x8000_0000_0000_0080,
    0x0000_0000_0000_800A,
    0x8000_0000_8000_000A,
    0x8000_0000_8000_8081,
    0x8000_0000_0000_8080,
    0x0000_0000_8000_0001,
    0x8000_0000_8000_8008,
];

const ROTATION_CONSTANTS: [u64; 24] = [
    1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 2, 14, 27, 41, 56, 8, 25, 43, 62, 18, 39, 61, 20, 44,
];

const PI: [usize; 24] = [
    10, 7, 11, 17, 18, 3, 5, 16, 8, 21, 24, 4, 15, 23, 19, 13, 12, 2, 20, 14, 22, 9, 6, 1,
];

const SHAKE128_RATE: usize = 168;
const SHAKE256_RATE: usize = 136;
const SHA3_224_RATE: usize = 144;
const SHA3_256_RATE: usize = 136;
const SHA3_384_RATE: usize = 104;
const SHA3_512_RATE: usize = 72;

const SHA3_BYTE_SEPARATOR: u8 = 0x06;
const SHAKE_BYTE_SEPARATOR: u8 = 0x1F;

#[inline]
fn store64(byte_array: &mut [u8], num: u64) {
    byte_array
        .iter_mut()
        .enumerate()
        .for_each(|(i, byte)| *byte = (num >> (i * 8) & 0xffu64) as u8);
}

#[inline]
fn load64(byte_array: &[u8]) -> u64 {
    byte_array
        .iter()
        .enumerate()
        .fold(0u64, |mut result, (i, byte)| {
            result |= u64::from(*byte) << (8 * i);
            result
        })
}
//fn load64<'a, T>(byte_iterator: T) -> u64
//where
//    T: IntoIterator<Item = &'a u8>,
//{
//    byte_iterator
//        .into_iter()
//        .enumerate()
//        .fold(0u64, |mut result, (i, byte)| {
//            result |= u64::from(*byte) << (8 * i);
//            result
//        })
//}

#[derive(Debug)]
pub struct Keccak {
    state: [u64; 25],
    byte_separation: u8,
    rate: usize,
}

macro_rules! keccak_constructor {
    ($name:ident, $rate:expr, $byte_sep:expr) => {
        #[inline]
        pub fn $name() -> Self {
            Self {
                state: [0; 25],
                byte_separation: $byte_sep,
                rate: $rate,
            }
        }
    };
}

impl Keccak {
    keccak_constructor!(sha3_224, SHA3_224_RATE, SHA3_BYTE_SEPARATOR);
    keccak_constructor!(sha3_256, SHA3_256_RATE, SHA3_BYTE_SEPARATOR);
    keccak_constructor!(sha3_384, SHA3_384_RATE, SHA3_BYTE_SEPARATOR);
    keccak_constructor!(sha3_512, SHA3_512_RATE, SHA3_BYTE_SEPARATOR);
    keccak_constructor!(shake128, SHAKE128_RATE, SHAKE_BYTE_SEPARATOR);
    keccak_constructor!(shake256, SHAKE256_RATE, SHAKE_BYTE_SEPARATOR);

    pub fn absorb(&mut self, input: &[u8]) {
        // Clear state
        self.state.iter_mut().for_each(|state_byte| *state_byte = 0);

        // 136 bytes is the maximum padding possible
        let mut padding = [0u8; 136];
        // Pad input to meet absorption rate
        let padding_len = self.pad(input.len(), &mut padding);

        // Baseline => This has a heap allocation but somehow has better performance than all other
        // implementations I tried. TODO Need to figure out a way to remove the heap allocation and
        // still have half decent performance, i.e., it should have better performance.
        let padded_input_iter: Vec<u8> = [input, &padding].concat();

        let rate = self.rate;
        for i in 0..((input.len() + padding_len) / rate) {
            for j in 0..(rate >> 3) {
                self.state[j] ^= load64(&padded_input_iter[(j * 8 + i * rate)..][..8]);
            }
            self.keccakf1600();
        }
    }

    pub fn squeeze(&mut self, buffer: &mut [u8]) {
        (0..(self.rate >> 3)).for_each(|j| store64(&mut buffer[(j * 8)..][..8], self.state[j]));

        for i in 1..(buffer.len() / self.rate) {
            self.keccakf1600();
            (0..(self.rate >> 3))
                .for_each(|j| store64(&mut buffer[(j * 8 + i * self.rate)..][..8], self.state[j]));
        }
    }

    #[inline]
    fn pad(&self, input_len: usize, padding: &mut [u8]) -> usize {
        let padding_len = self.rate - (input_len % self.rate);

        padding[0] ^= self.byte_separation;
        padding[padding_len - 1] ^= 0x80;

        padding_len
    }

    fn keccakf1600(&mut self) {
        (0..24).for_each(|i| self.round(ROUND_CONSTANTS[i]));
    }

    #[inline]
    fn round(&mut self, round_constant: u64) {
        // θ step
        let tmp: [u64; 5] = [
            self.state[0] ^ self.state[5] ^ self.state[10] ^ self.state[15] ^ self.state[20],
            self.state[1] ^ self.state[6] ^ self.state[11] ^ self.state[16] ^ self.state[21],
            self.state[2] ^ self.state[7] ^ self.state[12] ^ self.state[17] ^ self.state[22],
            self.state[3] ^ self.state[8] ^ self.state[13] ^ self.state[18] ^ self.state[23],
            self.state[4] ^ self.state[9] ^ self.state[14] ^ self.state[19] ^ self.state[24],
        ];

        // This loop is somehow faster than looping the contents of self.state and zipping tmp.
        (0..5).for_each(|x| {
            self.state[x] ^= tmp[(x + 4) % 5] ^ tmp[(x + 1) % 5].rotate_left(1);
            self.state[x + 5] ^= tmp[(x + 4) % 5] ^ tmp[(x + 1) % 5].rotate_left(1);
            self.state[x + 10] ^= tmp[(x + 4) % 5] ^ tmp[(x + 1) % 5].rotate_left(1);
            self.state[x + 15] ^= tmp[(x + 4) % 5] ^ tmp[(x + 1) % 5].rotate_left(1);
            self.state[x + 20] ^= tmp[(x + 4) % 5] ^ tmp[(x + 1) % 5].rotate_left(1);
        });

        // ρ and π steps
        // Needed to unroll this manually since the compiler couldn't achieve the best
        // optimisiations.
        let a1_old = self.state[1];
        self.state[PI[23]] = self.state[PI[22]].rotate_left(ROTATION_CONSTANTS[23] as u32);
        self.state[PI[22]] = self.state[PI[21]].rotate_left(ROTATION_CONSTANTS[22] as u32);
        self.state[PI[21]] = self.state[PI[20]].rotate_left(ROTATION_CONSTANTS[21] as u32);
        self.state[PI[20]] = self.state[PI[19]].rotate_left(ROTATION_CONSTANTS[20] as u32);
        self.state[PI[19]] = self.state[PI[18]].rotate_left(ROTATION_CONSTANTS[19] as u32);
        self.state[PI[18]] = self.state[PI[17]].rotate_left(ROTATION_CONSTANTS[18] as u32);
        self.state[PI[17]] = self.state[PI[16]].rotate_left(ROTATION_CONSTANTS[17] as u32);
        self.state[PI[16]] = self.state[PI[15]].rotate_left(ROTATION_CONSTANTS[16] as u32);
        self.state[PI[15]] = self.state[PI[14]].rotate_left(ROTATION_CONSTANTS[15] as u32);
        self.state[PI[14]] = self.state[PI[13]].rotate_left(ROTATION_CONSTANTS[14] as u32);
        self.state[PI[13]] = self.state[PI[12]].rotate_left(ROTATION_CONSTANTS[13] as u32);
        self.state[PI[12]] = self.state[PI[11]].rotate_left(ROTATION_CONSTANTS[12] as u32);
        self.state[PI[11]] = self.state[PI[10]].rotate_left(ROTATION_CONSTANTS[11] as u32);
        self.state[PI[10]] = self.state[PI[9]].rotate_left(ROTATION_CONSTANTS[10] as u32);
        self.state[PI[9]] = self.state[PI[8]].rotate_left(ROTATION_CONSTANTS[9] as u32);
        self.state[PI[8]] = self.state[PI[7]].rotate_left(ROTATION_CONSTANTS[8] as u32);
        self.state[PI[7]] = self.state[PI[6]].rotate_left(ROTATION_CONSTANTS[7] as u32);
        self.state[PI[6]] = self.state[PI[5]].rotate_left(ROTATION_CONSTANTS[6] as u32);
        self.state[PI[5]] = self.state[PI[4]].rotate_left(ROTATION_CONSTANTS[5] as u32);
        self.state[PI[4]] = self.state[PI[3]].rotate_left(ROTATION_CONSTANTS[4] as u32);
        self.state[PI[3]] = self.state[PI[2]].rotate_left(ROTATION_CONSTANTS[3] as u32);
        self.state[PI[2]] = self.state[PI[1]].rotate_left(ROTATION_CONSTANTS[2] as u32);
        self.state[PI[1]] = self.state[PI[0]].rotate_left(ROTATION_CONSTANTS[1] as u32);
        self.state[PI[0]] = a1_old.rotate_left(ROTATION_CONSTANTS[0] as u32);

        // χ step
        (0..25).step_by(5).for_each(|y| {
            let old_a_y = self.state[y];
            let old_a_y_1 = self.state[y + 1];

            self.state[y] ^= !self.state[y + 1] & self.state[y + 2];
            self.state[y + 1] ^= !self.state[y + 2] & self.state[y + 3];
            self.state[y + 2] ^= !self.state[y + 3] & self.state[y + 4];
            self.state[y + 3] ^= !self.state[y + 4] & old_a_y;
            self.state[y + 4] ^= !old_a_y & old_a_y_1;
        });

        // ι step
        self.state[0] ^= round_constant;
    }
}

macro_rules! sha3_wrapper {
    ($instance:ident, $rate:expr, $output_size:expr) => {
        pub fn $instance(input: &[u8], result: &mut [u8; $output_size]) {
            let mut sha3 = Keccak::$instance();
            sha3.absorb(&input);
            let mut final_result = [0u8; $rate];
            sha3.squeeze(&mut final_result);

            result
                .iter_mut()
                .zip(final_result.iter())
                .for_each(|(result_byte, final_result_byte)| *result_byte = *final_result_byte);
        }
    };
}

sha3_wrapper!(sha3_224, SHA3_224_RATE, 28);
sha3_wrapper!(sha3_256, SHA3_256_RATE, 32);
sha3_wrapper!(sha3_384, SHA3_384_RATE, 48);
sha3_wrapper!(sha3_512, SHA3_512_RATE, 64);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tiny_keccak_example_readme() {
        let mut res = [0u8; 32];
        let input: &[u8] = "hello world".as_bytes();

        sha3_256(&input, &mut res);

        let expected: &[u8] = &[
            0x64, 0x4b, 0xcc, 0x7e, 0x56, 0x43, 0x73, 0x04, 0x09, 0x99, 0xaa, 0xc8, 0x9e, 0x76,
            0x22, 0xf3, 0xca, 0x71, 0xfb, 0xa1, 0xd9, 0x72, 0xfd, 0x94, 0xa3, 0x1c, 0x3b, 0xfb,
            0xf2, 0x4e, 0x39, 0x38,
        ];

        assert_eq!(&res, expected);
    }

    #[test]
    fn empty_sha3_256() {
        let mut res = [0u8; 32];

        sha3_256(&[], &mut res);

        let expected = [
            0xa7, 0xff, 0xc6, 0xf8, 0xbf, 0x1e, 0xd7, 0x66, 0x51, 0xc1, 0x47, 0x56, 0xa0, 0x61,
            0xd6, 0x62, 0xf5, 0x80, 0xff, 0x4d, 0xe4, 0x3b, 0x49, 0xfa, 0x82, 0xd8, 0x0a, 0x4b,
            0x80, 0xf8, 0x43, 0x4a,
        ];

        let ref_ex: &[u8] = &expected;
        assert_eq!(&res, ref_ex);
    }

    #[test]
    fn string_sha3_256() {
        let mut res = [0u8; 32];
        let data: &[u8] = "hello".as_bytes();

        sha3_256(&data, &mut res);

        let expected = [
            0x33, 0x38, 0xbe, 0x69, 0x4f, 0x50, 0xc5, 0xf3, 0x38, 0x81, 0x49, 0x86, 0xcd, 0xf0,
            0x68, 0x64, 0x53, 0xa8, 0x88, 0xb8, 0x4f, 0x42, 0x4d, 0x79, 0x2a, 0xf4, 0xb9, 0x20,
            0x23, 0x98, 0xf3, 0x92,
        ];

        let ref_ex: &[u8] = &expected;
        assert_eq!(&res, ref_ex);
    }

    #[test]
    fn string_sha3_256_lorem() {
        let mut res = [0u8; 32];
        let data: &[u8] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
                           sed do eiusmod tempor incididunt ut labore et dolore magna \
                           aliqua. Ut enim ad minim veniam, quis nostrud exercitation \
                           ullamco laboris nisi ut aliquip ex ea commodo consequat. \
                           Duis aute irure dolor in reprehenderit in voluptate velit \
                           esse cillum dolore eu fugiat nulla pariatur. Excepteur \
                           sint occaecat cupidatat non proident, sunt in culpa qui \
                           officia deserunt mollit anim id est laborum."
            .as_bytes();

        sha3_256(&data, &mut res);

        let expected = [
            0xbd, 0xe3, 0xf2, 0x69, 0x17, 0x5e, 0x1d, 0xcd, 0xa1, 0x38, 0x48, 0x27, 0x8a, 0xa6,
            0x04, 0x6b, 0xd6, 0x43, 0xce, 0xa8, 0x5b, 0x84, 0xc8, 0xb8, 0xbb, 0x80, 0x95, 0x2e,
            0x70, 0xb6, 0xea, 0xe0,
        ];

        let ref_ex: &[u8] = &expected;
        assert_eq!(&res, ref_ex);
    }

    #[test]
    fn lorem_sha3_512() {
        let mut res = [0u8; 64];
        let data: &[u8] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
                           sed do eiusmod tempor incididunt ut labore et dolore magna \
                           aliqua. Ut enim ad minim veniam, quis nostrud exercitation \
                           ullamco laboris nisi ut aliquip ex ea commodo consequat. \
                           Duis aute irure dolor in reprehenderit in voluptate velit \
                           esse cillum dolore eu fugiat nulla pariatur. Excepteur \
                           sint occaecat cupidatat non proident, sunt in culpa qui \
                           officia deserunt mollit anim id est laborum."
            .as_bytes();

        sha3_512(&data, &mut res);

        let expected = [
            0xf3, 0x2a, 0x94, 0x23, 0x55, 0x13, 0x51, 0xdf, 0x0a, 0x07, 0xc0, 0xb8, 0xc2, 0x0e,
            0xb9, 0x72, 0x36, 0x7c, 0x39, 0x8d, 0x61, 0x06, 0x60, 0x38, 0xe1, 0x69, 0x86, 0x44,
            0x8e, 0xbf, 0xbc, 0x3d, 0x15, 0xed, 0xe0, 0xed, 0x36, 0x93, 0xe3, 0x90, 0x5e, 0x9a,
            0x8c, 0x60, 0x1d, 0x9d, 0x00, 0x2a, 0x06, 0x85, 0x3b, 0x97, 0x97, 0xef, 0x9a, 0xb1,
            0x0c, 0xbd, 0xe1, 0x00, 0x9c, 0x7d, 0x0f, 0x09,
        ];

        let ref_res: &[u8] = &res;
        let ref_ex: &[u8] = &expected;
        assert_eq!(ref_res, ref_ex);
    }

    #[test]
    fn lorem_sha3_384() {
        let mut res = [0u8; 48];
        let data: &[u8] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
                           sed do eiusmod tempor incididunt ut labore et dolore magna \
                           aliqua. Ut enim ad minim veniam, quis nostrud exercitation \
                           ullamco laboris nisi ut aliquip ex ea commodo consequat. \
                           Duis aute irure dolor in reprehenderit in voluptate velit \
                           esse cillum dolore eu fugiat nulla pariatur. Excepteur \
                           sint occaecat cupidatat non proident, sunt in culpa qui \
                           officia deserunt mollit anim id est laborum."
            .as_bytes();

        sha3_384(&data, &mut res);

        let expected = [
            0xe2, 0x97, 0xfd, 0x85, 0xa7, 0x7f, 0xe4, 0xf0, 0x00, 0x57, 0x85, 0xb8, 0x30, 0xdc,
            0x8e, 0x87, 0x2f, 0xb3, 0xb5, 0xf3, 0x34, 0x9c, 0x01, 0x81, 0xe4, 0xd0, 0xe4, 0xc5,
            0xad, 0x67, 0x75, 0x12, 0x49, 0x7d, 0x5c, 0xfe, 0x08, 0xe7, 0x53, 0xbe, 0xe7, 0x06,
            0x26, 0xba, 0x96, 0xa4, 0x7d, 0x35,
        ];

        let ref_res: &[u8] = &res;
        let ref_ex: &[u8] = &expected;
        assert_eq!(ref_res, ref_ex);
    }

    #[test]
    fn lorem_sha3_224() {
        let mut res = [0u8; 28];
        let data: &[u8] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
                           sed do eiusmod tempor incididunt ut labore et dolore magna \
                           aliqua. Ut enim ad minim veniam, quis nostrud exercitation \
                           ullamco laboris nisi ut aliquip ex ea commodo consequat. \
                           Duis aute irure dolor in reprehenderit in voluptate velit \
                           esse cillum dolore eu fugiat nulla pariatur. Excepteur \
                           sint occaecat cupidatat non proident, sunt in culpa qui \
                           officia deserunt mollit anim id est laborum."
            .as_bytes();

        sha3_224(&data, &mut res);

        let expected = [
            0x06, 0x77, 0x4e, 0x56, 0xa3, 0x76, 0xc3, 0xde, 0x43, 0x1f, 0x20, 0xd1, 0x76, 0x0c,
            0x28, 0x9e, 0xc0, 0x7c, 0xe8, 0xa4, 0x20, 0xe4, 0xc9, 0xb1, 0xc0, 0x8c, 0xbc, 0x16,
        ];

        let ref_res: &[u8] = &res;
        let ref_ex: &[u8] = &expected;
        assert_eq!(ref_res, ref_ex);
    }

    #[test]
    fn lorem_shake128() {
        let data: &[u8] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
                           sed do eiusmod tempor incididunt ut labore et dolore magna \
                           aliqua. Ut enim ad minim veniam, quis nostrud exercitation \
                           ullamco laboris nisi ut aliquip ex ea commodo consequat. \
                           Duis aute irure dolor in reprehenderit in voluptate velit \
                           esse cillum dolore eu fugiat nulla pariatur. Excepteur \
                           sint occaecat cupidatat non proident, sunt in culpa qui \
                           officia deserunt mollit anim id est laborum."
            .as_bytes();

        let mut shake = Keccak::shake128();
        let mut res = [0u8; 168];

        shake.absorb(&data);
        shake.squeeze(&mut res);

        let expected = [
            0x4d, 0xfb, 0xd3, 0x32, 0x68, 0x85, 0xc9, 0x38, 0x9e, 0xdc, 0xbf, 0x02, 0x8d, 0x04,
            0xc6, 0x2c, 0x21, 0x78, 0xcc, 0x11, 0xfc, 0x65, 0xec, 0xed, 0x30, 0xfc, 0x78, 0x79,
            0x44, 0x55, 0xfe, 0xb6, 0xf5, 0x2f, 0x2d, 0xa9, 0x3d, 0xfc, 0x09, 0xd7, 0x21, 0xff,
            0xaf, 0xb4, 0x4c, 0x9f, 0x29, 0x9b, 0xa0, 0x4e, 0x42, 0x86, 0xae, 0x6f, 0x92, 0xc4,
            0x17, 0x7e, 0x45, 0xbe, 0x95, 0x5f, 0xd9, 0xa0, 0xa0, 0xe6, 0x72, 0xe6, 0xc4, 0xd8,
            0xc8, 0x30, 0xf6, 0xe1, 0x39, 0x45, 0x16, 0xbf, 0x2a, 0xcf, 0x40, 0x1b, 0x12, 0x9a,
            0xd2, 0x88, 0x7e, 0x98, 0xe3, 0x51, 0x0b, 0xa5, 0x4d, 0x73, 0xc9, 0x89, 0xa3, 0x3a,
            0xa8, 0xae, 0xd0, 0xc8, 0x78, 0x2d, 0x17, 0x14, 0x8a, 0x43, 0xc3, 0x49, 0xc2, 0x98,
            0x0e, 0xc8, 0x3f, 0x74, 0x7b, 0xf7, 0xd3, 0x36, 0x8a, 0x8c, 0x23, 0x1d, 0x49, 0x0f,
            0x42, 0x58, 0x8c, 0x1d, 0xbf, 0x75, 0x9b, 0xb8, 0x2a, 0xa9, 0x99, 0xe0, 0x1a, 0xe7,
            0xa7, 0xdb, 0x27, 0x26, 0x8d, 0xef, 0x0a, 0x5d, 0xa9, 0x41, 0x3b, 0x82, 0x5e, 0xe6,
            0x70, 0x8d, 0x94, 0xd0, 0x4d, 0x83, 0xf1, 0xaa, 0x9b, 0x11, 0x0b, 0x26, 0xb5, 0x8f,
        ];

        let ref_res: &[u8] = &res;
        let ref_ex: &[u8] = &expected;
        assert_eq!(ref_res, ref_ex);
    }

    #[test]
    fn lorem_shake256() {
        let data: &[u8] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, \
                           sed do eiusmod tempor incididunt ut labore et dolore magna \
                           aliqua. Ut enim ad minim veniam, quis nostrud exercitation \
                           ullamco laboris nisi ut aliquip ex ea commodo consequat. \
                           Duis aute irure dolor in reprehenderit in voluptate velit \
                           esse cillum dolore eu fugiat nulla pariatur. Excepteur \
                           sint occaecat cupidatat non proident, sunt in culpa qui \
                           officia deserunt mollit anim id est laborum."
            .as_bytes();

        let mut shake = Keccak::shake256();
        let mut res = [0u8; 136];

        shake.absorb(&data);
        shake.squeeze(&mut res);

        let expected = [
            0x83, 0x94, 0x9e, 0x0d, 0x68, 0xf6, 0x0b, 0x17, 0x1f, 0xb7, 0xf3, 0x90, 0x9b, 0xe5,
            0xae, 0xba, 0x6e, 0x63, 0xaf, 0x63, 0x77, 0x18, 0x9f, 0xe8, 0x8b, 0xa9, 0xb3, 0xeb,
            0x6b, 0xdc, 0xb9, 0xfb, 0xff, 0x13, 0xc4, 0xb3, 0x40, 0x48, 0xef, 0x0c, 0x7e, 0x3f,
            0x02, 0x5a, 0x8b, 0x1a, 0xe7, 0xd5, 0xde, 0xe4, 0xcf, 0xe0, 0xe0, 0xbf, 0xfb, 0xb2,
            0x98, 0xdf, 0x51, 0x33, 0x6f, 0x1a, 0x67, 0xc4, 0x4a, 0xe0, 0x80, 0xdb, 0x95, 0xd5,
            0xb8, 0x3d, 0x60, 0xcc, 0x9f, 0xc6, 0xf4, 0xb4, 0x6b, 0xee, 0xa3, 0x9f, 0xa5, 0x7d,
            0x9b, 0xc1, 0x52, 0x57, 0xe9, 0x86, 0x3a, 0x16, 0x8b, 0x87, 0xa0, 0xe4, 0x67, 0x94,
            0xae, 0x23, 0x05, 0x5a, 0x01, 0x30, 0xa8, 0x2c, 0x70, 0x97, 0x46, 0xb2, 0x14, 0x03,
            0x89, 0x3f, 0x6e, 0x4d, 0xa6, 0x68, 0x4f, 0xe0, 0xb7, 0x26, 0x14, 0x0d, 0x79, 0xe7,
            0x1b, 0x59, 0xa9, 0xae, 0x16, 0x64, 0x75, 0xad, 0x4f, 0xe5,
        ];

        let ref_res: &[u8] = &res;
        let ref_ex: &[u8] = &expected;
        assert_eq!(ref_res, ref_ex);
    }
}
