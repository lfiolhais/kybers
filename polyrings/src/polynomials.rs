use super::modulo::Modulo;
use super::rings::Ring;

use std::fmt;
use std::iter::{DoubleEndedIterator, IntoIterator};
use std::ops::{Add, Index, IndexMut, Sub};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Polynomial {
    coeffs: Vec<Modulo>,
    q: u64,
}

impl Polynomial {
    /// Create a polynomial for a given ring Rq
    pub fn new(ring: &Ring) -> Self {
        Polynomial {
            coeffs: vec![Modulo::new(ring.get_poly_mod() as u64, 0); ring.get_max_poly_order()],
            q: ring.get_poly_mod() as u64,
        }
    }

    /// Create a polynomial for a given ring Rq with unitialized data
    pub fn new_unitialized(ring: &Ring) -> Self {
        Polynomial {
            coeffs: Vec::with_capacity(ring.get_max_poly_order()),
            q: ring.get_poly_mod() as u64,
        }
    }

    /// Create a polynomial from specific parts
    pub fn from_parts(q: u64, size: usize) -> Self {
        Polynomial {
            coeffs: vec![Modulo::new(q, 0); size],
            q,
        }
    }

    /// Create a polynomial from specific parts
    pub fn from_parts_unitialized(q: u64, size: usize) -> Self {
        Polynomial {
            coeffs: Vec::with_capacity(size),
            q,
        }
    }

    /// Get length of the polynomial
    pub fn len(&self) -> usize {
        self.coeffs.len()
    }

    /// Swap two coefficient locations in the polynomial
    pub fn swap(&mut self, a: usize, b: usize) {
        self.coeffs.swap(a, b);
    }

    /// Store a coefficient in the polynomial at a specific index
    pub fn insert(&mut self, index: usize, coeff: Modulo) {
        self.coeffs[index] = coeff;
    }

    /// Push coefficient into the polynomial
    pub fn push(&mut self, coeff: Modulo) {
        self.coeffs.push(coeff);
    }

    /// Create a reference iterator over the coefficients of the current Polynomial
    pub fn iter(&self) -> PolyIter {
        PolyIter {
            coeffs_iter: self.coeffs.iter(),
        }
    }

    /// Create a mutable reference iterator over the coefficients of the current Polynomial
    pub fn iter_mut(&mut self) -> PolyIterMut {
        PolyIterMut {
            coeffs_iter: self.coeffs.iter_mut(),
        }
    }

    /// Return a slice of all coefficients present in this polynomial
    pub fn as_slice(&self) -> &[Modulo] {
        self.coeffs.as_slice()
    }

    /// Decompress a 64-bit array to one or more Polynomials in Rq
    ///
    /// # Arguments
    /// * `to_decompress` => 64-bit array which contains the coefficients for the polynomial
    /// * `d` => compression factor
    /// * `ring` => Reference to ring which defines Rq
    ///
    /// # Return Value
    /// Vector of polynomials in Rq
    pub fn decompress(to_decompress: &[Polynomial], d: usize, ring: &Ring, k: usize) -> Vec<Self> {
        let mut out: Vec<Self> = vec![Self::new(ring); k];

        out.iter_mut()
            .zip(to_decompress.iter())
            .for_each(|(out_poly, poly)| {
                out_poly
                    .iter_mut()
                    .zip(poly.iter())
                    .for_each(|(out_coeff, coeff)| {
                        let tmp = (ring.get_poly_mod() as f32 / 2_u64.pow(d as u32) as f32)
                            * coeff.get_num() as f32;
                        let round = if tmp.fract() >= 0.5 {
                            tmp.ceil() as i64
                        } else {
                            tmp.floor() as i64
                        };
                        *out_coeff = Modulo::new(ring.get_poly_mod() as u64, round);
                    });
            });

        out
    }

    /// Deserialize byte array which was encoded using 1-bit segments to a polynomial in Rq.
    ///
    /// # Arguments
    /// `bytes_to_decode` => polynomial to decode.
    ///
    /// # Return Value
    /// Vector of polynomial coefficients in the Ring.
    pub fn decode1(bytes_to_decode: &[u8], poly_size: usize, mod_q: u64) -> Self {
        let mut out = Self::from_parts_unitialized(mod_q, poly_size);
        let decompress_val = {
            let tmp = mod_q as f32 / 2 as f32;
            if tmp.fract() >= 0.5 {
                tmp.ceil() as i64
            } else {
                tmp.floor() as i64
            }
        };

        bytes_to_decode.iter().for_each(|coeffs| {
            out.push(Modulo::new(mod_q, (coeffs & 0x01) as i64 * decompress_val));
            out.push(Modulo::new(
                mod_q,
                ((coeffs & 0x02) >> 1) as i64 * decompress_val,
            ));
            out.push(Modulo::new(
                mod_q,
                ((coeffs & 0x04) >> 2) as i64 * decompress_val,
            ));
            out.push(Modulo::new(
                mod_q,
                ((coeffs & 0x08) >> 3) as i64 * decompress_val,
            ));
            out.push(Modulo::new(
                mod_q,
                ((coeffs & 0x10) >> 4) as i64 * decompress_val,
            ));
            out.push(Modulo::new(
                mod_q,
                ((coeffs & 0x20) >> 5) as i64 * decompress_val,
            ));
            out.push(Modulo::new(
                mod_q,
                ((coeffs & 0x40) >> 6) as i64 * decompress_val,
            ));
            out.push(Modulo::new(
                mod_q,
                ((coeffs & 0x80) >> 7) as i64 * decompress_val,
            ));
        });

        out
    }

    pub fn encode1(polynomials: &[Self]) -> Vec<u8> {
        let mut out: Vec<u8> = vec![];

        polynomials.iter().for_each(|poly| {
            poly.as_slice().chunks(8).for_each(|coeffs| {
                let byte = (coeffs[0].get_num()) as u8
                    | (coeffs[1].get_num() << 1) as u8
                    | (coeffs[2].get_num() << 2) as u8
                    | (coeffs[3].get_num() << 3) as u8
                    | (coeffs[4].get_num() << 4) as u8
                    | (coeffs[5].get_num() << 5) as u8
                    | (coeffs[6].get_num() << 6) as u8
                    | (coeffs[7].get_num() << 7) as u8;
                out.push(byte);
            });
        });

        out
    }

    /// Deserialize byte array which was encoded using 11-bit segments to a polynomial in Rq.
    ///
    /// # Arguments
    /// `bytes_to_decode` => polynomial to decode.
    ///
    /// # Panics
    /// When `bytes_to_decode` can't be decoded into a polynomial in Rq.
    ///
    /// # Return Value
    /// Vector of polynomial coefficients in the Ring.
    pub fn decode11(ring: &Ring, bytes_to_decode: &[u8], k: usize) -> Vec<Self> {
        //assert!(
        //    bytes_to_decode.len() % 4 == 0,
        //    "Decode: the rand_bytes array passed is not a multiple of 32"
        //);

        let mut out = vec![
            Polynomial::from_parts_unitialized(
                ring.get_poly_mod() as u64,
                ring.get_max_poly_order()
            );
            k
        ];

        bytes_to_decode
            .chunks(11)
            .enumerate()
            .for_each(|(i, coeffs)| {
                let serialized = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[1]) & 0x07) << 8) | (i64::from(coeffs[0]) & 0xff),
                );
                out[i / 32].push(serialized);
                let serialized_2 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[2]) & 0x3f) << 5) | ((i64::from(coeffs[1]) & 0xf8) >> 3),
                );
                out[i / 32].push(serialized_2);
                let serialized_3 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[4]) & 0x01) << 10)
                        | ((i64::from(coeffs[3]) & 0xff) << 2)
                        | ((i64::from(coeffs[2]) & 0xc0) >> 6),
                );
                out[i / 32].push(serialized_3);
                let serialized_4 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[5]) & 0x0f) << 7) | ((i64::from(coeffs[4]) & 0xfe) >> 1),
                );
                out[i / 32].push(serialized_4);
                let serialized_5 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[6]) & 0x7f) << 4) | ((i64::from(coeffs[5]) & 0xf0) >> 4),
                );
                out[i / 32].push(serialized_5);
                let serialized_6 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[8]) & 0x03) << 9)
                        | ((i64::from(coeffs[7]) & 0xff) << 1)
                        | ((i64::from(coeffs[6]) & 0x80) >> 7),
                );
                out[i / 32].push(serialized_6);
                let serialized_7 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[9]) & 0x1f) << 6) | ((i64::from(coeffs[8]) & 0xfc) >> 2),
                );
                out[i / 32].push(serialized_7);
                let serialized_8 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[10]) & 0xff) << 3) | ((i64::from(coeffs[9]) & 0xe0) >> 5),
                );
                out[i / 32].push(serialized_8);
            });

        out
    }

    /// Encode one or more polynomials into 11-bit segments
    ///
    /// # Arguments
    /// * `poly_to_encode` => Polynomial to encode
    ///
    /// # Panics
    /// When the polynomial length to encode isn't a multiple of 8.
    ///
    /// # Return Value
    /// Array of bytes with the polynomial encoded
    pub fn encode11(polynomials: &[Self]) -> Vec<u8> {
        let mut out: Vec<u8> = Vec::with_capacity(polynomials[0].len() * 11);

        polynomials.iter().for_each(|poly_to_encode| {
            poly_to_encode.as_slice().chunks(8).for_each(|coeffs| {
                let serialized = (coeffs[0].get_num() & 0xff) as u8;
                out.push(serialized);
                let rem_serialized = ((coeffs[0].get_num() >> 8) & 0x07) as u8;
                let serialized_2 = (coeffs[1].get_num() << 3) as u8 | rem_serialized;
                out.push(serialized_2);
                let rem_next_serial = ((coeffs[1].get_num() >> 5) & 0x3f) as u8;
                let serialized_3 = (coeffs[2].get_num() << 6) as u8 | rem_next_serial;
                out.push(serialized_3);
                let serialized_4 = ((coeffs[2].get_num() >> 2) & 0xff) as u8;
                out.push(serialized_4);
                let rem_next_serial = ((coeffs[2].get_num() >> 10) & 0x01) as u8;
                let serialized_5 = (coeffs[3].get_num() << 1) as u8 | rem_next_serial;
                out.push(serialized_5);
                let rem_next_serial = ((coeffs[3].get_num() >> 7) & 0x0f) as u8;
                let serialized_6 = (coeffs[4].get_num() << 4) as u8 | rem_next_serial;
                out.push(serialized_6);
                let rem_next_serial = ((coeffs[4].get_num() >> 4) & 0x7f) as u8;
                let serialized_7 = (coeffs[5].get_num() << 7) as u8 | rem_next_serial;
                out.push(serialized_7);
                let serialized_8 = ((coeffs[5].get_num() >> 1) & 0xff) as u8;
                out.push(serialized_8);
                let rem_next_serial = ((coeffs[5].get_num() >> 9) & 0x03) as u8;
                let serialized_9 = (coeffs[6].get_num() << 2) as u8 | rem_next_serial;
                out.push(serialized_9);
                let rem_next_serial = ((coeffs[6].get_num() >> 6) & 0x1f) as u8;
                let serialized_10 = (coeffs[7].get_num() << 5) as u8 | rem_next_serial;
                out.push(serialized_10);
                let serialized_11 = ((coeffs[7].get_num() >> 3) & 0xff) as u8;
                out.push(serialized_11);
            });
        });

        out
    }

    /// Compress a polynomial in Rq into an array of integers.
    ///
    /// # Arguments
    /// `polynomial_vec` => polynomial to compress.
    /// `d` => compression factor
    ///
    /// # Return Value
    /// Vector of compressed integers.
    fn compress_polynomial(&self, d: usize) -> Polynomial {
        let mut out = Polynomial::from_parts_unitialized(self.q, self.len());

        self.iter().for_each(|x| {
            let tmp = (2_u64.pow(d as u32) as f32 / self.q as f32) * x.get_num() as f32;
            let round = if tmp.fract() >= 0.5 {
                tmp.ceil() as u64
            } else {
                tmp.floor() as u64
            };
            out.push(Modulo::new(self.q, (round % 2_u64.pow(d as u32)) as i64));
        });

        out
    }

    /// Compress an array of polynomials in Rq into an array of integers.
    ///
    /// # Arguments
    /// `polynomial_vec` => polynomial to compress.
    /// `d` => compression factor
    ///
    /// # Return Value
    /// Vector of compressed integers.
    pub fn compress(polynomials: &[Self], d: usize, k: usize) -> Vec<Polynomial> {
        let mut out =
            vec![Polynomial::from_parts_unitialized(polynomials[0].q, polynomials[0].len()); k];

        polynomials.iter().enumerate().for_each(|(i, poly)| {
            let tmp = poly.compress_polynomial(d);
            out[i] = tmp;
        });

        out
    }

    /// Encode one or more polynomials into 13-bit segments. We assume all polynomials have the
    /// same length, and the array of polynomials isn't empty.
    ///
    /// # Arguments
    /// * `poly_to_encode` => Polynomials to encode
    ///
    /// # Panics
    /// * When the polynomial to encode isn't a multiple of 8.
    /// * The array of polynomials is empty.
    ///
    /// # Return Value
    /// Array of bytes with the polynomials encoded.
    pub fn encode13(polynomials: &[Polynomial]) -> Vec<u8> {
        let mut out: Vec<u8> = Vec::with_capacity(polynomials[0].len() as usize * 13);

        polynomials.iter().for_each(|poly| {
            poly.as_slice().chunks(8).for_each(|coeffs| {
                out.push((coeffs[0].get_num() & 0xff) as u8);
                let rem_serial = ((coeffs[0].get_num() >> 8) & 0x1f) as u8;
                let serialized_2 = ((coeffs[1].get_num() & 0x07) << 5) as u8 | rem_serial;
                out.push(serialized_2);
                out.push(((coeffs[1].get_num() >> 3) & 0xff) as u8);
                let rem_serial = ((coeffs[1].get_num() >> 11) & 0x03) as u8;
                let serialized_4 = ((coeffs[2].get_num() & 0x3f) << 2) as u8 | rem_serial;
                out.push(serialized_4);
                let rem_serial = ((coeffs[2].get_num() >> 6) & 0x7f) as u8;
                let serialized_5 = ((coeffs[3].get_num() & 0x01) << 7) as u8 | rem_serial;
                out.push(serialized_5);
                out.push(((coeffs[3].get_num() >> 1) & 0xff) as u8);
                let rem_serial = ((coeffs[3].get_num() >> 9) & 0x0f) as u8;
                let serialized_7 = ((coeffs[4].get_num() & 0x0f) << 4) as u8 | rem_serial;
                out.push(serialized_7);
                out.push(((coeffs[4].get_num() >> 4) & 0xff) as u8);
                let rem_serial = ((coeffs[4].get_num() >> 12) & 0x01) as u8;
                let serialized_9 = ((coeffs[5].get_num() & 0x7f) << 1) as u8 | rem_serial;
                out.push(serialized_9);
                let rem_serial = ((coeffs[5].get_num() >> 7) & 0x3f) as u8;
                let serialized_10 = ((coeffs[6].get_num() & 0x03) << 6) as u8 | rem_serial;
                out.push(serialized_10);
                out.push(((coeffs[6].get_num() >> 2) & 0xff) as u8);
                let rem_serial = ((coeffs[6].get_num() >> 10) & 0x07) as u8;
                let serialized_12 = ((coeffs[7].get_num() & 0x1f) << 3) as u8 | rem_serial;
                out.push(serialized_12);
                out.push(((coeffs[7].get_num() >> 5) & 0xff) as u8);
            });
        });

        out
    }

    /// Deserialize byte array which was encoded using 13-bit segments to a polynomial in Rq.
    ///
    /// # Arguments
    /// `bytes_to_decode` => polynomial to decode.
    ///
    /// # Panics
    /// When `bytes_to_decode` can't be decoded into a polynomial in Rq.
    ///
    /// # Return Value
    /// Vector of polynomial coefficients in the Ring.
    pub fn decode13(ring: &Ring, bytes_to_decode: &[u8], k: usize) -> Vec<Self> {
        //assert!(
        //    bytes_to_decode.len() % 4 == 0,
        //    "Decode: the rand_bytes array passed is not a multiple of 32"
        //);

        let mut poly = vec![
            Polynomial::from_parts_unitialized(
                ring.get_poly_mod() as u64,
                ring.get_max_poly_order(),
            );
            k
        ];

        bytes_to_decode
            .chunks(13)
            .enumerate()
            .for_each(|(i, coeffs)| {
                let decoded = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[1]) & 0x1f) << 8) | (i64::from(coeffs[0]) & 0xff),
                );
                poly[i / 32].push(decoded);
                let decoded_2 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[3]) & 0x03) << 11)
                        | ((i64::from(coeffs[2]) & 0xff) << 3)
                        | ((i64::from(coeffs[1]) & 0xe0) >> 5),
                );
                poly[i / 32].push(decoded_2);
                let decoded_3 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[4]) & 0x7f) << 6) | ((i64::from(coeffs[3]) & 0xfc) >> 2),
                );
                poly[i / 32].push(decoded_3);
                let decoded_4 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[6]) & 0x0f) << 9)
                        | ((i64::from(coeffs[5]) & 0xff) << 1)
                        | ((i64::from(coeffs[4]) & 0x80) >> 7),
                );
                poly[i / 32].push(decoded_4);
                let decoded_5 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[8]) & 0x01) << 12)
                        | ((i64::from(coeffs[7]) & 0xff) << 4)
                        | ((i64::from(coeffs[6]) & 0xf0) >> 4),
                );
                poly[i / 32].push(decoded_5);
                let decoded_6 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[9]) & 0x3f) << 7) | ((i64::from(coeffs[8]) & 0xfe) >> 1),
                );
                poly[i / 32].push(decoded_6);
                let decoded_7 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[11]) & 0x07) << 10)
                        | ((i64::from(coeffs[10]) & 0xff) << 2)
                        | ((i64::from(coeffs[9]) & 0xc0) >> 6),
                );
                poly[i / 32].push(decoded_7);
                let decoded_8 = Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[12]) & 0xff) << 5) | ((i64::from(coeffs[11]) & 0xf8) >> 3),
                );
                poly[i / 32].push(decoded_8);
            });

        poly
    }

    /// Encode one or more polynomials into 3-bit segments
    ///
    /// # Arguments
    /// * `poly_to_encode` => Polynomial to encode
    ///
    /// # Panics
    /// When the polynomial to encode isn't a multiple of 8.
    ///
    /// # Return Value
    /// Array of bytes with the polynomial encoded
    pub fn encode3(polynomials: &[Self]) -> Vec<u8> {
        let mut out: Vec<u8> = Vec::with_capacity(polynomials[0].len() * 3);

        polynomials.iter().for_each(|poly_to_encode| {
            poly_to_encode.as_slice().chunks(8).for_each(|coeffs| {
                out.push(
                    ((coeffs[2].get_num() & 0x03) << 6) as u8
                        | ((coeffs[1].get_num() & 0x07) << 3) as u8
                        | (coeffs[0].get_num() & 0x07) as u8,
                );
                out.push(
                    ((coeffs[5].get_num() & 0x01) << 7) as u8
                        | ((coeffs[4].get_num() & 0x07) << 4) as u8
                        | ((coeffs[3].get_num() & 0x07) << 1) as u8
                        | ((coeffs[2].get_num() & 0x04) >> 2) as u8,
                );
                out.push(
                    ((coeffs[7].get_num() & 0x07) << 5) as u8
                        | ((coeffs[6].get_num() & 0x07) << 2) as u8
                        | ((coeffs[5].get_num() & 0x06) >> 1) as u8,
                );
            });
        });

        out
    }

    /// Deserialize byte array which was encoded using 3-bit segments to a polynomial in Rq.
    ///
    /// # Arguments
    /// `bytes_to_decode` => polynomial to decode.
    ///
    /// # Panics
    /// When `bytes_to_decode` can't be decoded into a polynomial in Rq.
    ///
    /// # Return Value
    /// Vector of polynomial coefficients in the Ring.
    pub fn decode3(ring: &Ring, bytes_to_decode: &[u8], k: usize) -> Vec<Self> {
        //assert!(
        //    bytes_to_decode.len() % 4 == 0,
        //    "Decode: the rand_bytes array passed is not a multiple of 32"
        //);

        let mut out = vec![
            Polynomial::from_parts_unitialized(
                ring.get_poly_mod() as u64,
                ring.get_max_poly_order()
            );
            k
        ];

        bytes_to_decode
            .chunks(3)
            .enumerate()
            .for_each(|(i, coeffs)| {
                out[i / 32].push(Modulo::new(
                    ring.get_poly_mod() as u64,
                    i64::from(coeffs[0]) & 0x07,
                ));
                out[i / 32].push(Modulo::new(
                    ring.get_poly_mod() as u64,
                    (i64::from(coeffs[0]) & 0x38) >> 3,
                ));
                out[i / 32].push(Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[1]) & 0x01) << 2) | ((i64::from(coeffs[0]) & 0xc0) >> 6),
                ));
                out[i / 32].push(Modulo::new(
                    ring.get_poly_mod() as u64,
                    (i64::from(coeffs[1]) & 0x0e) >> 1,
                ));
                out[i / 32].push(Modulo::new(
                    ring.get_poly_mod() as u64,
                    (i64::from(coeffs[1]) & 0x70) >> 4,
                ));
                out[i / 32].push(Modulo::new(
                    ring.get_poly_mod() as u64,
                    ((i64::from(coeffs[2]) & 0x03) << 1) | ((i64::from(coeffs[1]) & 0x80) >> 7),
                ));
                out[i / 32].push(Modulo::new(
                    ring.get_poly_mod() as u64,
                    (i64::from(coeffs[2]) & 0x1c) >> 2,
                ));
                out[i / 32].push(Modulo::new(
                    ring.get_poly_mod() as u64,
                    (i64::from(coeffs[2]) & 0xe0) >> 5,
                ));
            });

        out
    }
}

impl Add for Polynomial {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        assert!(
            self.len() == rhs.len(),
            "Can't add polynomials of different orders"
        );
        let mut result = Polynomial::from_parts(self.q, self.len());
        result
            .iter_mut()
            .zip(self.iter())
            .zip(rhs.iter())
            .for_each(|((coeff, a), b)| *coeff = *a + *b);

        result
    }
}

impl Sub for Polynomial {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        assert!(
            self.len() == rhs.len(),
            "Can't add polynomials of different orders"
        );
        let mut result = Polynomial::from_parts(self.q, self.len());
        result
            .iter_mut()
            .zip(self.iter())
            .zip(rhs.iter())
            .for_each(|((coeff, a), b)| *coeff = *a - *b);

        result
    }
}

impl fmt::Display for Polynomial {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[")?;
        for (i, coeff) in self.coeffs.iter().enumerate() {
            write!(f, "{}", coeff.get_num())?;
            if i != (self.coeffs.len() - 1) {
                write!(f, ", ")?;
            }
        }

        write!(f, "] mod {}", self.q)
    }
}

impl IntoIterator for Polynomial {
    type Item = Modulo;
    type IntoIter = ::std::vec::IntoIter<Modulo>;

    fn into_iter(self) -> Self::IntoIter {
        self.coeffs.into_iter()
    }
}

impl<'a> IntoIterator for &'a Polynomial {
    type Item = &'a Modulo;
    type IntoIter = ::std::slice::Iter<'a, Modulo>;

    fn into_iter(self) -> Self::IntoIter {
        self.coeffs.iter()
    }
}

impl<'a> IntoIterator for &'a mut Polynomial {
    type Item = &'a mut Modulo;
    type IntoIter = ::std::slice::IterMut<'a, Modulo>;

    fn into_iter(self) -> Self::IntoIter {
        self.coeffs.iter_mut()
    }
}

impl Index<usize> for Polynomial {
    type Output = Modulo;

    fn index(&self, order: usize) -> &Self::Output {
        &self.coeffs[order]
    }
}

impl IndexMut<usize> for Polynomial {
    fn index_mut(&mut self, order: usize) -> &mut Modulo {
        &mut self.coeffs[order]
    }
}

#[derive(Debug, Clone)]
pub struct PolyIter<'a> {
    coeffs_iter: ::std::slice::Iter<'a, Modulo>,
}

impl<'a> Iterator for PolyIter<'a> {
    type Item = &'a Modulo;

    fn next(&mut self) -> Option<Self::Item> {
        self.coeffs_iter.next()
    }
}

impl<'a> DoubleEndedIterator for PolyIter<'a> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.coeffs_iter.next_back()
    }
}

#[derive(Debug)]
pub struct PolyIterMut<'a> {
    coeffs_iter: ::std::slice::IterMut<'a, Modulo>,
}

impl<'a> Iterator for PolyIterMut<'a> {
    type Item = &'a mut Modulo;

    fn next(&mut self) -> Option<Self::Item> {
        self.coeffs_iter.next()
    }
}

impl<'a> DoubleEndedIterator for PolyIterMut<'a> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.coeffs_iter.next_back()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_polynomial_iterator() {
        let mut poly = Polynomial::from_parts(16, 6);

        poly.insert(0, Modulo::new(16, 0));
        poly.insert(1, Modulo::new(16, 1));
        poly.insert(2, Modulo::new(16, 2));
        poly.insert(3, Modulo::new(16, 3));
        poly.insert(4, Modulo::new(16, 4));
        poly.insert(5, Modulo::new(16, 5));

        for (i, coeff) in poly.iter().enumerate() {
            assert_eq!(coeff.get_num(), i as u64);
        }
    }

    #[test]
    fn test_encode13() {
        let mut poly = Polynomial::from_parts_unitialized(8142, 8);
        poly.push(Modulo::new(8142, 0x400));
        poly.push(Modulo::new(8142, 0x02a));
        poly.push(Modulo::new(8142, 0x1ff));
        poly.push(Modulo::new(8142, 0x285));
        poly.push(Modulo::new(8142, 0xfa6));
        poly.push(Modulo::new(8142, 0x0ea));
        poly.push(Modulo::new(8142, 0x040));
        poly.push(Modulo::new(8142, 0x007));

        assert_eq!(
            Polynomial::encode13(&[poly]),
            [0x00, 0x44, 0x05, 0xfc, 0x87, 0x42, 0x61, 0xfa, 0xd4, 0x01, 0x10, 0x38, 0x00]
        );
    }

    #[test]
    fn test_decode13() {
        let mut poly = Polynomial::from_parts_unitialized(8142, 8);
        poly.push(Modulo::new(8142, 0x400));
        poly.push(Modulo::new(8142, 0x02a));
        poly.push(Modulo::new(8142, 0x1ff));
        poly.push(Modulo::new(8142, 0x285));
        poly.push(Modulo::new(8142, 0xfa6));
        poly.push(Modulo::new(8142, 0x0ea));
        poly.push(Modulo::new(8142, 0x040));
        poly.push(Modulo::new(8142, 0x007));
        let poly_expected = poly.clone();

        let poly_encoded = Polynomial::encode13(&[poly]);
        let ring = Ring::new(8, 8142, 3);

        assert_eq!(
            Polynomial::decode13(&ring, &poly_encoded, 1).remove(0),
            poly_expected
        );
    }

}
