//! The `polyrings` library provides an API to perform operations on polynomial
//! rings over a defined Polynomial Ring Zq[X]/(X^n + 1).
#[macro_use]
extern crate lazy_static;

pub mod modulo;
mod params;
pub mod polynomials;
pub mod rings;
