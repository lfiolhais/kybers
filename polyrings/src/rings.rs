//! The `rings` module provides a struct to build Polynomial Rings Zq[X]/(X^n + 1) with modulo `q`
//! and maximum polynomial order `n`.
//!
//! The Ring implementation defined herein provides an iterator which is able to generate random
//! coefficients for the specified Ring.
//!
//! *Warning*: This implementation is only able to generate Rings with coefficients smaller than
//! your CPU's maximum word size.
use super::modulo::Modulo;
use super::params::{INV_NTT, NTT};
use super::polynomials::Polynomial;

/// Define a Polynomial Ring Zq[X]/(X^n + 1)
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Ring {
    /// Maximum Polynomial order
    n: usize,
    /// Modulo of the integer polynomial ring
    q: usize,
    /// Bit mask used to perform the modulo operation when generating the coefficients for the
    /// specified Polynomial Ring.
    bit_mask: usize,
    /// Offset of the byte to inspect to make sure the generated coefficient conforms to the Ring
    /// specification.
    byte_to_inspect: usize,
    /// Number of bits to add to generate coefficient in the central binomial distribution
    niu: usize,
    /// Bit mask used when performing the sample in the central binomial distribution
    niu_bit_mask: u64,
}

impl Ring {
    /// Create a new Polynomial Ring with the specified modulo (`q`) and
    /// maximum polynomial order (`n`).
    pub fn new(n: usize, q: usize, niu: usize) -> Self {
        let n_bits_q = (q as f32).log2().ceil();
        let byte_to_inspect = (n_bits_q / 8.).ceil() as usize;
        let base_niu_bit_mask = (1 << niu) - 1;
        let niu_bit_mask: u64 =
            (0..4)
                .zip((0..64).step_by(niu))
                .fold(0u64, |mut result, (_, i)| {
                    result |= base_niu_bit_mask << (i + i);
                    result
                });

        Ring {
            n,
            q,
            niu,
            bit_mask: n_bits_q.exp2() as usize - 1,
            byte_to_inspect,
            niu_bit_mask,
        }
    }

    /// Get maximum polynomial order in Rq
    #[inline]
    pub fn get_max_poly_order(&self) -> usize {
        self.n
    }

    /// Get polynomial modulo in Rq
    #[inline]
    pub fn get_poly_mod(&self) -> usize {
        self.q
    }

    /// Uniformly sample random polynomial coefficients in defined ring Rq
    /// from a random byte array. The function does not guarantee there will
    /// be a coefficient for each order of the polynomial.
    ///
    /// # Arguments
    /// `rand_bytes` => a byte array of random data
    ///
    /// # Return Value
    /// Vector of polynomial coefficients in the Ring.
    pub fn gen_coefficients(&self, rand_bytes: &[u8]) -> Polynomial {
        let mut poly = Polynomial::new_unitialized(&self);

        rand_bytes
            .iter()
            .enumerate()
            .skip(self.byte_to_inspect - 1)
            .step_by(self.byte_to_inspect)
            .map(|(i, _)| {
                let mut res: usize = 0;
                for w in 0..self.byte_to_inspect {
                    res |= (rand_bytes[i - w] as usize)
                        << ((self.byte_to_inspect - 1 - w) << 3) as usize;
                }
                res & self.bit_mask
            })
            .filter(|coeff| *coeff < self.q)
            .enumerate()
            .for_each(|(i, coeff)| {
                if i < self.n {
                    poly.push(Modulo::new(self.q as u64, coeff as i64));
                }
            });

        poly
    }

    /// Sample values in the defined Ring from a centered binomial distribution.
    ///
    /// # Arguments
    /// `rand_bytes` => a byte array of random data, must be a multiple of 64.
    ///
    /// # Panics
    /// When `rand_bytes` is not a multiple of 64.
    ///
    /// # Return Value
    /// Vector of polynomial coefficients in the Ring.
    pub fn cbd(&self, rand_bytes: &[u8]) -> Polynomial {
        let mut poly = Polynomial::new(&self);

        poly.iter_mut().enumerate().for_each(|(i, coeff)| {
            let index = (i / (64 / (2 * (self.niu + 2)))) * self.niu;
            let eight_bytes = (0..5).fold(0u64, |mut result, w| {
                result |= u64::from(rand_bytes[index + w]) << (8 * w);
                result
            });

            let shift_base = (i % (64 / (2 * (self.niu + 2)))) * self.niu * 2;
            let tmp_a = eight_bytes >> shift_base & self.niu_bit_mask;
            let tmp_b = eight_bytes >> (shift_base + self.niu) & self.niu_bit_mask;

            // Accumulate bits in tmp_a and tmp_b
            let (a, b) = (0..self.niu).fold((0i32, 0i32), |(mut a, mut b), w| {
                a += (tmp_a >> w & 0x1) as i32;
                b += (tmp_b >> w & 0x1) as i32;
                (a, b)
            });

            // Generate coefficient
            *coeff = Modulo::new(self.q as u64, i64::from(a - b));
        });

        poly
    }

    /// Execute the NTT on a list of polynomials
    ///
    /// # Arguments
    /// * `polynomials` => list of polynomials where the NTT will be applied
    ///
    /// # Return Value
    /// The result of the NTT
    pub fn ntt(&self, polynomials: &[Polynomial]) -> Vec<Polynomial> {
        // Kyber only values
        let w = Modulo::new(self.q as u64, 3844);
        let phi = Modulo::new(self.q as u64, 62);
        // Kyber only values

        //let ntt: Vec<Modulo> = (0..(self.n * self.n))
        //    .map(|i| w.pow(((i / self.n) * (i % self.n)) as i64))
        //    .collect();
        //println!("ntt: {:?}", ntt);
        let phi_list: Vec<Modulo> = (0..self.n).map(|i| phi.pow(i as i64)).collect();

        let mut out_polys: Vec<Polynomial> = vec![Polynomial::new(&self); polynomials.len()];

        for (k, poly) in polynomials.iter().enumerate() {
            for i in 0..self.n {
                let mut sum = poly[0];
                for j in 1..self.n {
                    sum += phi_list[j] * poly[j] * NTT[j * self.n + i];
                }
                out_polys[k][i] = sum;
            }
        }

        out_polys
    }

    /// Execute the inverse NTT on a list of polynomials
    ///
    /// # Arguments
    /// * `polynomials` => list of polynomials where the inverse NTT will be applied
    ///
    /// # Return Value
    /// The result of the inverse NTT
    pub fn inv_ntt(&self, polynomials: &[Polynomial]) -> Vec<Polynomial> {
        // Kyber only values
        let w = Modulo::new(self.q as u64, 3844);
        let phi = Modulo::new(self.q as u64, 62);
        // Kyber only values

        //let inv_ntt: Vec<Modulo> = (0..(self.n * self.n))
        //    .map(|i| w.pow(-(((i / self.n) * (i % self.n)) as i64)))
        //    .collect();
        //println!("inv_ntt: {:?}", inv_ntt);
        let inv_phi_list: Vec<Modulo> = (0..self.n).map(|i| phi.pow(-(i as i64))).collect();
        let inv_n = Modulo::new(self.q as u64, self.n as i64).pow(-1);

        let mut out_polys: Vec<Polynomial> = vec![Polynomial::new(&self); polynomials.len()];

        for (k, poly) in polynomials.iter().enumerate() {
            for i in 0..self.n {
                let mut sum = poly[0];
                for j in 1..self.n {
                    sum += poly[j] * INV_NTT[j * self.n + i];
                }
                out_polys[k][i] = sum * inv_phi_list[i] * inv_n;
            }
        }

        out_polys
    }
}

#[cfg(test)]
mod tests {
    extern crate rand;

    use super::*;
    use rand::prelude::*;

    #[test]
    fn ntt_test() {
        // Kyber Parameters
        let n: usize = 256;
        let q: usize = 7681;
        let niu = 3;
        let kyber_ring = Ring::new(n, q, niu);

        let mut poly = Polynomial::new(&kyber_ring);
        poly.insert(0, Modulo::new(kyber_ring.q as u64, 1424));
        poly.insert(1, Modulo::new(kyber_ring.q as u64, 1234));
        poly.insert(2, Modulo::new(kyber_ring.q as u64, 23));
        poly.insert(3, Modulo::new(kyber_ring.q as u64, 59));
        poly.insert(4, Modulo::new(kyber_ring.q as u64, 7680));
        poly.insert(5, Modulo::new(kyber_ring.q as u64, 12));

        let poly_ntt = kyber_ring.ntt(&[poly]);

        let mut poly_expected = Polynomial::new(&kyber_ring);
        poly_expected.insert(0, Modulo::new(kyber_ring.q as u64, 168));
        poly_expected.insert(1, Modulo::new(kyber_ring.q as u64, 3680));
        poly_expected.insert(2, Modulo::new(kyber_ring.q as u64, 5291));
        poly_expected.insert(3, Modulo::new(kyber_ring.q as u64, 1623));
        poly_expected.insert(4, Modulo::new(kyber_ring.q as u64, 6429));
        poly_expected.insert(5, Modulo::new(kyber_ring.q as u64, 692));

        assert_eq!(poly_ntt, [poly_expected]);
    }

    #[test]
    fn ntt_full_cycle_test() {
        // Kyber Parameters
        let n: usize = 256;
        let q: usize = 7681;
        let niu = 3;
        let kyber_ring = Ring::new(n, q, niu);

        let mut poly = Polynomial::new(&kyber_ring);
        poly.insert(0, Modulo::new(kyber_ring.q as u64, 1424));
        poly.insert(1, Modulo::new(kyber_ring.q as u64, 1234));
        poly.insert(2, Modulo::new(kyber_ring.q as u64, 23));
        poly.insert(3, Modulo::new(kyber_ring.q as u64, 59));
        poly.insert(4, Modulo::new(kyber_ring.q as u64, 7680));
        poly.insert(5, Modulo::new(kyber_ring.q as u64, 12));

        let poly_final = kyber_ring
            .inv_ntt(&kyber_ring.ntt(&[poly.clone()]))
            .remove(0);

        assert_eq!(poly_final, poly);
    }

    #[test]
    fn new_kyber_ring_niu_3() {
        // Kyber Parameters
        let n: usize = 256;
        let q: usize = 7681;
        let niu = 3;
        let kyber_ring = Ring::new(n, q, niu);

        assert_eq!(
            kyber_ring,
            Ring {
                n,
                q,
                bit_mask: 8191,
                byte_to_inspect: 2,
                niu,
                niu_bit_mask: 0x0000_0000_001C_71C7
            }
        );
    }

    #[test]
    fn new_kyber_ring_niu_4() {
        // Kyber Parameters
        let n: usize = 256;
        let q: usize = 7681;
        let niu = 4;
        let kyber_ring = Ring::new(n, q, niu);

        assert_eq!(
            kyber_ring,
            Ring {
                n,
                q,
                bit_mask: 8191,
                byte_to_inspect: 2,
                niu,
                niu_bit_mask: 0x0000_0000_0F0F_0F0F
            }
        );
    }

    #[test]
    fn new_kyber_ring_niu_5() {
        // Kyber Parameters
        let n: usize = 256;
        let q: usize = 7681;
        let niu = 5;
        let kyber_ring = Ring::new(n, q, niu);

        assert_eq!(
            kyber_ring,
            Ring {
                n,
                q,
                bit_mask: 8191,
                byte_to_inspect: 2,
                niu,
                niu_bit_mask: 0x0000_0007_C1F0_7C1F
            }
        );
    }

    #[test]
    fn generate_coefficients() {
        let mut rng = thread_rng();
        const N_BYTES: usize = 256;
        let mut random_bytes: [u8; N_BYTES] = [0; N_BYTES];

        (0..N_BYTES).for_each(|i| {
            random_bytes[i] = rng.gen();
        });

        let kyber_ring = Ring::new(256, 7681, 5);
        let coeffs = kyber_ring.gen_coefficients(&random_bytes);

        for (i, coeff) in coeffs.iter().enumerate() {
            assert!(
                coeff.get_num() < kyber_ring.q as u64,
                "Coeff[{}] {} is larger than specificed q {}",
                i,
                coeff,
                kyber_ring.q
            );
        }
    }

    #[test]
    fn cbd_with_niu_5() {
        let mut rng = thread_rng();
        const N_BYTES: usize = 348;
        let mut random_bytes: [u8; N_BYTES] = [0; N_BYTES];

        (0..N_BYTES).for_each(|i| {
            random_bytes[i] = rng.gen();
        });

        let kyber_ring = Ring::new(256, 7681, 5);
        let coeffs = kyber_ring.cbd(&random_bytes);

        for (i, coeff) in coeffs.iter().enumerate() {
            assert!(
                coeff.get_num() < kyber_ring.q as u64,
                "Coeff[{}] {} is larger than specificed q {}",
                i,
                coeff,
                kyber_ring.q
            );
        }
    }

    #[test]
    fn cbd_with_niu_4() {
        let mut rng = thread_rng();
        const N_BYTES: usize = 256;
        let mut random_bytes: [u8; N_BYTES] = [0; N_BYTES];

        (0..N_BYTES).for_each(|i| {
            random_bytes[i] = rng.gen();
        });

        let kyber_ring = Ring::new(256, 7681, 4);
        let coeffs = kyber_ring.cbd(&random_bytes);

        for (i, coeff) in coeffs.iter().enumerate() {
            assert!(
                coeff.get_num() < kyber_ring.get_poly_mod() as u64,
                "Coeff[{}] {} is larger than specificed q {}",
                i,
                coeff,
                kyber_ring.q
            );
        }
    }

    #[test]
    fn cbd_with_niu_3() {
        let mut rng = thread_rng();
        const N_BYTES: usize = 208;
        let mut random_bytes: [u8; N_BYTES] = [0; N_BYTES];

        (0..N_BYTES).for_each(|i| {
            random_bytes[i] = rng.gen();
        });

        let kyber_ring = Ring::new(256, 7681, 3);
        let poly = kyber_ring.cbd(&random_bytes);

        assert!(
            poly.len() != 0 && poly.len() == 256,
            "Polynomial doesn't have 256 elements as requested"
        );

        for (i, coeff) in poly.iter().enumerate() {
            assert!(
                coeff.get_num() < kyber_ring.get_poly_mod() as u64,
                "Coeff[{}] {} is larger than specificed q {}",
                i,
                coeff,
                kyber_ring.q
            );
        }
    }
}
