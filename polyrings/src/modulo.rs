//! The `modulo` module provides a basic struct to perform modulo arithmetic.
use std::fmt;
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct Modulo {
    /// Base of the arithmetic
    q: u64,
    /// Number in the modular base
    num: u64,
}

impl Modulo {
    pub fn new(q: u64, num: i64) -> Self {
        Modulo {
            q,
            num: ((num % q as i64) as i64 + q as i64) as u64 % q,
        }
    }

    pub fn get_num(&self) -> u64 {
        self.num
    }

    fn egcd(a: i64, b: i64) -> (i64, i64) {
        let mut s = 0;
        let mut remainder = b;

        let mut old_s = 1;
        let mut old_remainder = a;

        while remainder != 0 {
            let quotient = old_remainder / remainder;
            let tmp = remainder;
            remainder = old_remainder - quotient * remainder;
            old_remainder = tmp;
            let tmp = s;
            s = old_s - quotient * s;
            old_s = tmp;
        }

        (old_remainder, old_s)
    }

    pub fn pow(self, exp: i64) -> Self {
        if exp == 0 {
            return Modulo::new(self.q, 1);
        }

        let mut pow_result = self;
        let n_pow = exp.abs();
        for _ in 0..(n_pow - 1) {
            pow_result *= self;
        }

        if exp < 0 {
            pow_result = Modulo::new(self.q, 1) / pow_result;
        }

        pow_result
    }
}

impl fmt::Display for Modulo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} mod {}", self.num, self.q)
    }
}

impl Add for Modulo {
    type Output = Self;

    fn add(self, rhs: Modulo) -> Self::Output {
        assert_eq!(self.q, rhs.q);

        let num: i64 = self.num as i64 + rhs.num as i64;
        Modulo {
            q: self.q,
            num: ((num % self.q as i64) as i64 + self.q as i64) as u64 % self.q,
        }
    }
}

impl AddAssign for Modulo {
    fn add_assign(&mut self, rhs: Modulo) {
        *self = *self + rhs;
    }
}

impl Sub for Modulo {
    type Output = Self;

    fn sub(self, rhs: Modulo) -> Self::Output {
        assert_eq!(self.q, rhs.q);

        let num: i64 = self.num as i64 - rhs.num as i64;
        Modulo {
            q: self.q,
            num: ((num % self.q as i64) as i64 + self.q as i64) as u64 % self.q,
        }
    }
}

impl SubAssign for Modulo {
    fn sub_assign(&mut self, rhs: Modulo) {
        *self = *self - rhs;
    }
}

impl Mul for Modulo {
    type Output = Self;

    fn mul(self, rhs: Modulo) -> Self::Output {
        assert_eq!(self.q, rhs.q);

        let num: i64 = self.num as i64 * rhs.num as i64;
        Modulo {
            q: self.q,
            num: ((num % self.q as i64) as i64 + self.q as i64) as u64 % self.q,
        }
    }
}

impl MulAssign for Modulo {
    fn mul_assign(&mut self, rhs: Modulo) {
        *self = *self * rhs;
    }
}

#[allow(clippy::suspicious_arithmetic_impl)]
impl Div for Modulo {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        assert_eq!(self.q, rhs.q);

        let (gcd, x) = Modulo::egcd(rhs.num as i64, rhs.q as i64);
        assert!(gcd == 1, "Modular inverse does not exist");

        let inv_rhs = Modulo {
            q: self.q,
            num: (x % rhs.q as i64) as u64,
        };

        self * inv_rhs
    }
}

impl DivAssign for Modulo {
    fn div_assign(&mut self, rhs: Modulo) {
        *self = *self / rhs;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_modulo_number() {
        let my_num = Modulo::new(3, 5);
        assert_eq!(my_num, Modulo { q: 3, num: 2 });
        let expr_num = Modulo::new(3, 5 + 16);
        assert_eq!(expr_num, Modulo { q: 3, num: 0 });
    }

    #[test]
    fn test_addition() {
        let my_num5 = Modulo::new(3, 5);
        let my_num14 = Modulo::new(3, 14);
        assert_eq!(my_num5 + my_num14, Modulo { q: 3, num: 1 });
        let my_num5 = Modulo::new(3, 5);
        let my_num14 = Modulo::new(3, -14);
        assert_eq!(my_num5 + my_num14, Modulo { q: 3, num: 0 });
        let my_num5 = Modulo::new(3, -5);
        let my_num14 = Modulo::new(3, 14);
        assert_eq!(my_num5 + my_num14, Modulo { q: 3, num: 0 });
        let my_num5 = Modulo::new(3, -5);
        let my_num14 = Modulo::new(3, -14);
        assert_eq!(my_num5 + my_num14, Modulo { q: 3, num: 2 });
    }

    #[test]
    #[should_panic]
    fn test_addition_with_different_modulo_base() {
        let my_num5 = Modulo::new(3, 5);
        let my_num14 = Modulo::new(4, 14);

        let _panic = my_num5 + my_num14;
    }

    #[test]
    fn test_subtraction() {
        let my_num14 = Modulo::new(3, 14);
        let my_num5 = Modulo::new(3, 5);
        assert_eq!(my_num14 - my_num5, Modulo { q: 3, num: 0 });
        let my_num5 = Modulo::new(3, 5);
        let my_num14 = Modulo::new(3, 13);
        assert_eq!(my_num5 - my_num14, Modulo { q: 3, num: 1 });
        let my_num5 = Modulo::new(3, -5);
        let my_num14 = Modulo::new(3, 13);
        assert_eq!(my_num5 - my_num14, Modulo { q: 3, num: 0 });
        let my_num5 = Modulo::new(3, -5);
        let my_num14 = Modulo::new(3, -13);
        assert_eq!(my_num5 - my_num14, Modulo { q: 3, num: 2 });
    }

    #[test]
    #[should_panic]
    fn test_subtraction_with_different_modulo_base() {
        let my_num5 = Modulo::new(3, 5);
        let my_num14 = Modulo::new(4, 14);

        let _panic = my_num5 - my_num14;
    }

    #[test]
    fn test_multiplication() {
        let my_num14 = Modulo::new(3, 14);
        let my_num5 = Modulo::new(3, 5);
        assert_eq!(my_num14 * my_num5, Modulo { q: 3, num: 1 });
        let my_num5 = Modulo::new(3, 5);
        let my_num14 = Modulo::new(3, 13);
        assert_eq!(my_num5 * my_num14, Modulo { q: 3, num: 2 });
        let my_num5 = Modulo::new(3, -5);
        let my_num14 = Modulo::new(3, 13);
        assert_eq!(my_num5 * my_num14, Modulo { q: 3, num: 1 });
        let my_num5 = Modulo::new(3, -5);
        let my_num14 = Modulo::new(3, -13);
        assert_eq!(my_num5 * my_num14, Modulo { q: 3, num: 2 });
    }

    #[test]
    fn test_division() {
        let my_num1 = Modulo::new(11, 1);
        let my_num3 = Modulo::new(11, 3);
        assert_eq!(my_num1 / my_num3, Modulo { q: 11, num: 4 });
        let my_num1 = Modulo::new(13, 1);
        let my_num8 = Modulo::new(13, 8);
        assert_eq!(my_num1 / my_num8, Modulo { q: 13, num: 5 });
        let my_num1 = Modulo::new(13, 1);
        let my_num4 = Modulo::new(13, 4);
        assert_eq!(my_num1 / my_num4, Modulo { q: 13, num: 10 });
        assert_eq!(
            my_num1 / my_num8 * my_num1 / my_num8,
            Modulo { q: 13, num: 12 }
        );
        assert_eq!(
            my_num1 / my_num8 * my_num1 / my_num8 * my_num1 / my_num8,
            Modulo { q: 13, num: 8 }
        );
        assert_eq!(
            my_num1 / my_num8 * my_num1 / my_num8 * my_num1 / my_num8 * my_num1 / my_num8,
            Modulo { q: 13, num: 1 }
        );
    }

    #[test]
    fn testing_powers() {
        let my_num8 = Modulo::new(13, 8);
        assert_eq!(my_num8.pow(-1), Modulo { q: 13, num: 5 });
        assert_eq!(my_num8.pow(-2), Modulo { q: 13, num: 12 });
        assert_eq!(my_num8.pow(-3), Modulo { q: 13, num: 8 });
        assert_eq!(my_num8.pow(-4), Modulo { q: 13, num: 1 });
        assert_eq!(my_num8.pow(2), Modulo { q: 13, num: 12 });
    }

    #[test]
    fn simple_ntt_and_inv_ntt() {
        let w = Modulo::new(13, 12);

        let mut inv_ntt = [[Modulo::new(13, 1); 2]; 2];
        let mut ntt = [[Modulo::new(13, 1); 2]; 2];

        for i in 1..2 {
            for j in 1..2 {
                inv_ntt[i][j] = w.pow(-(i as i64 * j as i64));
                ntt[i][j] = w.pow(i as i64 * j as i64);
            }
        }

        let ntt_expected = [
            [Modulo::new(13, 1), Modulo::new(13, 1)],
            [Modulo::new(13, 1), Modulo::new(13, 12)],
        ];

        let inv_ntt_expected = [
            [Modulo::new(13, 1), Modulo::new(13, 1)],
            [Modulo::new(13, 1), Modulo::new(13, 12)],
        ];

        // Compare ntt generation
        assert_eq!(inv_ntt_expected, inv_ntt);
        assert_eq!(ntt_expected, ntt);

        let poly_a = [Modulo::new(13, 10), Modulo::new(13, 2)]; // a = 10 + 2 * X

        let mut ntt_poly_a = [Modulo::new(13, 0); 2];

        // Vector Matrix multiplication
        for i in 0..2 {
            let mut sum_a = Modulo::new(13, 0);
            for j in 0..2 {
                sum_a += poly_a[j] * ntt[j][i];
            }
            ntt_poly_a[i] = sum_a;
        }

        assert_eq!(ntt_poly_a, [Modulo::new(13, 12), Modulo::new(13, 8)]);

        let mut inv_ntt_poly_a = [Modulo::new(13, 0); 2];

        // Vector Matrix multiplication
        for i in 0..2 {
            let mut sum_a = Modulo::new(13, 0);
            for j in 0..2 {
                sum_a += ntt_poly_a[j] * inv_ntt[j][i];
            }
            inv_ntt_poly_a[i] = sum_a * Modulo::new(13, 2).pow(-1);
        }

        assert_eq!(inv_ntt_poly_a, poly_a);
    }

    #[test]
    fn polynomial_multiplication() {
        let q = 1543;
        let poly_a = [
            Modulo::new(q, 10),
            Modulo::new(q, 2),
            Modulo::new(q, 16),
            Modulo::new(q, 0),
            Modulo::new(q, 0),
            Modulo::new(q, 0),
        ]; // a = 10 + 2 * X + 16 * X^2

        let poly_b = [
            Modulo::new(q, 1),
            Modulo::new(q, 5),
            Modulo::new(q, 11),
            Modulo::new(q, 0),
            Modulo::new(q, 0),
            Modulo::new(q, 0),
        ]; // b = 1 + 5 * X + 11 * X^2

        let w = Modulo::new(q, 682);

        let ntt: Vec<Modulo> = (0..6).map(|i| w.pow(i as i64)).collect();

        // Compare ntt generation
        assert_eq!(
            [
                Modulo::new(q, 1),
                Modulo::new(q, 682),
                Modulo::new(q, 681),
                Modulo::new(q, 1542),
                Modulo::new(q, 861),
                Modulo::new(q, 862),
            ],
            ntt.as_slice()
        );

        let mut ntt_poly_a = [Modulo::new(q, 0); 6];
        let mut ntt_poly_b = [Modulo::new(q, 0); 6];

        // Vector Matrix multiplication
        for i in 0..6 {
            let mut sum_a = Modulo::new(q, 0);
            let mut sum_b = Modulo::new(q, 0);
            for j in 0..6 {
                sum_a += poly_a[j] * ntt[(i * j) % 6];
                sum_b += poly_b[j] * ntt[(i * j) % 6];
            }
            ntt_poly_a[i] = sum_a;
            ntt_poly_b[i] = sum_b;
        }

        assert_eq!(
            ntt_poly_a,
            [
                Modulo::new(q, 28),
                Modulo::new(q, 1469),
                Modulo::new(q, 1261),
                Modulo::new(q, 24),
                Modulo::new(q, 284),
                Modulo::new(q, 80)
            ]
        );
        assert_eq!(
            ntt_poly_b,
            [
                Modulo::new(q, 17),
                Modulo::new(q, 101),
                Modulo::new(q, 533),
                Modulo::new(q, 7),
                Modulo::new(q, 996),
                Modulo::new(q, 1438)
            ]
        );

        let mut poly_c_prime = [Modulo::new(q, 0); 6];

        // Vector Matrix multiplication
        for i in 0..6 {
            poly_c_prime[i] = ntt_poly_a[i] * ntt_poly_b[i];
        }

        assert_eq!(
            poly_c_prime,
            [
                Modulo::new(q, 476),
                Modulo::new(q, 241),
                Modulo::new(q, 908),
                Modulo::new(q, 168),
                Modulo::new(q, 495),
                Modulo::new(q, 858),
            ]
        );

        let mut result_poly = [Modulo::new(q, 0); 6];
        for i in 0..6 {
            let mut sum = Modulo::new(q, 0);
            for j in 0..6 {
                sum += poly_c_prime[j] * ntt[(6 - ((i * j) % 6)) % 6];
            }
            result_poly[i] = Modulo::new(q, 6).pow(-1) * sum;
        }

        assert_eq!(
            result_poly,
            [
                Modulo::new(q, 10),
                Modulo::new(q, 52),
                Modulo::new(q, 136),
                Modulo::new(q, 102),
                Modulo::new(q, 176),
                Modulo::new(q, 0)
            ]
        );

        let mut orig_result_poly = [0u64; 6];
        result_poly
            .iter()
            .map(|modulo| modulo.num)
            .enumerate()
            .for_each(|(i, num)| {
                orig_result_poly[i] = num % q;
            });

        assert_eq!(orig_result_poly, [10, 52, 136, 102, 176, 0]);
    }
}
