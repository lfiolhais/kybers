extern crate kybers;

use kybers::Kyber;

use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let kyber = Kyber::v512();

    let (public_key, secret_key) = kyber.keygen();
    println!("Public Key size {}", public_key.len());
    println!("Public Key: {:02X?}", public_key);
    println!();
    println!("Secret Key size {}", secret_key.len());
    println!("Secret Key: {:02X?}", secret_key);

    let (cipher_text, shared_key) = kyber.encrypt(&public_key);
    println!("Cipher Text size {}", cipher_text.len());
    println!("Cipher Text: {:02X?}", cipher_text);
    println!();
    println!("Shared Key size {}", shared_key.len());
    println!("Shared Key: {:02X?}", shared_key);

    let shared_key_from_cipher = kyber.decrypt(&cipher_text, &secret_key);
    println!("Decrypted Shared Key size {}", shared_key_from_cipher.len());
    println!("Decrypted Shared Key: {:02X?}", shared_key_from_cipher);

    assert!(
        shared_key == shared_key_from_cipher,
        "Shared Keys are different"
    );

    Ok(())
}
