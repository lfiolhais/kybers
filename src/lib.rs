extern crate polyrings;
extern crate rand;
extern crate sha3;

use rand::prelude::*;

use polyrings::modulo::Modulo;
use polyrings::polynomials::Polynomial;
use polyrings::rings::Ring;

const KYBER_SYMBYTES: usize = 32;

const DT: usize = 11;
const DU: usize = 11;
const DV: usize = 3;

const N: u64 = 256;
const Q: u64 = 7681;

pub struct Kyber {
    /// Size of the polynomials in the lattice
    n: u64,
    /// Modulo of the Ring
    q: u64,
    /// Size of the lattice base matrix
    k: u64,
    /// Maximum size of the noise to be added to the public key
    niu: u64,
}

macro_rules! kyber_constructor {
    ($name:ident,$n:expr, $q:expr, $k:expr, $niu:expr) => {
        #[inline]
        pub fn $name() -> Self {
            Self {
                n: $n,
                q: $q,
                k: $k,
                niu: $niu,
            }
        }
    };
}

impl Kyber {
    kyber_constructor!(v512, N, Q, 2, 5);
    kyber_constructor!(v768, N, Q, 3, 4);
    kyber_constructor!(v1024, N, Q, 4, 3);

    /// Generate Public and Secret keys
    pub fn keygen(&self) -> (Vec<u8>, Vec<u8>) {
        let (public_key, secret_key) = self.keygen_pke();

        // Encapsulate the Secret Key
        let mut buf = [0u8; 32];
        sha3::sha3_256(&public_key, &mut buf);

        let mut rng = rand::thread_rng();
        let mut z = [0u8; 32];
        z.iter_mut().for_each(|byte| *byte = rng.gen());
        let secret_key = [secret_key, public_key.clone(), buf.to_vec(), z.to_vec()].concat();

        (public_key, secret_key)
    }

    /// Encrypt
    pub fn encrypt(&self, public_key: &[u8]) -> (Vec<u8>, Vec<u8>) {
        let mut rng = rand::thread_rng();
        let mut message = [0u8; 32];
        message.iter_mut().for_each(|byte| *byte = rng.gen());

        let mut hash_message = [0u8; 32];
        sha3::sha3_256(&message, &mut hash_message);

        let mut hash_public_key = [0u8; 32];
        sha3::sha3_256(&public_key, &mut hash_public_key);
        let mut k_r = [0u8; 64];
        sha3::sha3_512(&[hash_message, hash_public_key].concat(), &mut k_r);
        let (k_prime, random_coins) = k_r.split_at(KYBER_SYMBYTES);

        let cipher_text = self.encrypt_pke(&public_key, &hash_message, &random_coins);

        let mut cipher_text_hash = [0u8; 32];
        sha3::sha3_256(&cipher_text, &mut cipher_text_hash);

        let mut k = [0u8; 32];
        sha3::sha3_256(&[k_prime, &cipher_text_hash].concat(), &mut k);

        (cipher_text, k.to_vec())
    }

    pub fn decrypt(&self, cipher_text: &[u8], secret_key: &[u8]) -> Vec<u8> {
        let message_prime = self.decrypt_pke(
            &secret_key[..(13 * self.k as usize * self.n as usize / 8)],
            &cipher_text,
        );

        let hash_public_key = &secret_key[((13 + DT) * self.k as usize * self.n as usize / 8 + 32)
            ..((13 + DT) * self.k as usize * self.n as usize / 8 + 64)];
        let mut buf = [0u8; 64];
        sha3::sha3_512(&[&message_prime, hash_public_key].concat(), &mut buf);

        let (shared_key_prime, random_coins_prime) = buf.split_at(KYBER_SYMBYTES);

        let public_key = &secret_key[(13 * self.k as usize * self.n as usize / 8)
            ..((13 + DT) * self.k as usize * self.n as usize / 8 + 32)];
        let cipher_text_prime = self.encrypt_pke(public_key, &message_prime, random_coins_prime);

        let mut hashed_cipher_text = [0u8; 32];
        sha3::sha3_256(&cipher_text, &mut hashed_cipher_text);

        let mut buf2 = [0u8; 32];
        if cipher_text == cipher_text_prime.as_slice() {
            println!("Decryption succeeded");
            sha3::sha3_256(&[shared_key_prime, &hashed_cipher_text].concat(), &mut buf2);
        } else {
            println!("Decryption failed");
            let z = &secret_key[((13 + DT) * self.k as usize * self.n as usize / 8 + 64)..];
            sha3::sha3_256(&[z, &hashed_cipher_text].concat(), &mut buf2);
        }

        buf2.to_vec()
    }

    fn keygen_pke(&self) -> (Vec<u8>, Vec<u8>) {
        // Generate Random Bytes
        let mut rng = rand::thread_rng();
        let mut rand_byte_array = [0u8; 32];
        rand_byte_array
            .iter_mut()
            .for_each(|byte| *byte = rng.gen());

        // Generate Seed
        let mut buf = [0u8; 64];
        sha3::sha3_512(&rand_byte_array, &mut buf);
        let (public_seed, noise_seed) = buf.split_at(KYBER_SYMBYTES);

        let ring = Ring::new(self.n as usize, self.q as usize, self.niu as usize);

        // Sample from Lattice Space
        let a = self.gen_matrix_a(&ring, &public_seed, false);
        let (s, e) = self.random_samples_from_ring(&noise_seed, &ring, 0);

        // Generate Secret Key
        let mut s_ntt = ring.ntt(&s);
        Self::to_bit_reversed_order_polyvec(&mut s_ntt);

        // t = As + e
        let mut t_prime = self.matrix_vector_mul(&a, &s_ntt);
        Self::to_bit_reversed_order_polyvec(&mut t_prime);
        let inv_ntt_t_prime = ring.inv_ntt(&t_prime);
        let t = self.add_vectors(inv_ntt_t_prime, e);

        // Compress and encode t
        let t_encoded = Polynomial::encode11(&Polynomial::compress(&t, DT, self.k as usize));

        // Generate Public Key
        let public_key = [&t_encoded, public_seed].concat();

        // Encode Secret Key
        let secret_key = Polynomial::encode13(&s_ntt);

        (public_key, secret_key)
    }

    fn encrypt_pke(&self, public_key: &[u8], message: &[u8], random_coins: &[u8]) -> Vec<u8> {
        let ring = Ring::new(self.n as usize, self.q as usize, self.niu as usize);

        let pk_decoded = Polynomial::decode11(
            &ring,
            &public_key[..(DT * self.k as usize * (self.n >> 3) as usize)],
            self.k as usize,
        );
        let t = Polynomial::decompress(&pk_decoded, DT, &ring, self.k as usize);

        let m_decoded = Polynomial::decode1(message, self.n as usize, self.q);

        // This needs to be in bit-inverted order
        let mut t_ntt = ring.ntt(&t);
        Self::to_bit_reversed_order_polyvec(&mut t_ntt);

        let public_seed = &public_key[(DT * self.k as usize * (self.n >> 3) as usize)..];

        // Generate A transposed
        let a_transposed = self.gen_matrix_a(&ring, &public_seed, true);

        let (r, e1) = self.random_samples_from_ring(&random_coins, &ring, 0);

        // This needs to be in bit-inverted order
        let mut r_ntt = ring.ntt(&r);
        Self::to_bit_reversed_order_polyvec(&mut r_ntt);

        let (mut e2, _) =
            self.random_samples_from_ring(&random_coins, &ring, (self.k * 2) as usize);
        let e2 = e2.remove(0);

        let mut at_mul_rntt = self.matrix_vector_mul(&a_transposed, &r_ntt);
        Self::to_bit_reversed_order_polyvec(&mut at_mul_rntt);
        let u_prime = ring.inv_ntt(&at_mul_rntt);
        let u = self.add_vectors(u_prime, e1);

        let mut tt_mul_r_ntt = self.vector_vector_mul(&t_ntt, &r_ntt);
        Self::to_bit_reversed_order_poly(&mut tt_mul_r_ntt);
        let v_prime = ring.inv_ntt(&[tt_mul_r_ntt]).remove(0);
        let v = v_prime + e2 + m_decoded;

        let c1 = Polynomial::encode11(&Polynomial::compress(&u, DU, self.k as usize));
        let c2 = Polynomial::encode3(&Polynomial::compress(&[v], DV, 1));

        // Cipher Text
        [c1, c2].concat()
    }

    fn decrypt_pke(&self, secret_key: &[u8], cipher_text: &[u8]) -> Vec<u8> {
        let ring = Ring::new(self.n as usize, self.q as usize, self.niu as usize);

        let u_decoded = Polynomial::decode11(
            &ring,
            &cipher_text[..(DU * self.k as usize * self.n as usize / 8)],
            self.k as usize,
        );
        let u = Polynomial::decompress(&u_decoded, DU, &ring, self.k as usize);

        let v_decoded = Polynomial::decode3(
            &ring,
            &cipher_text[(DU * self.k as usize * self.n as usize / 8)..],
            1,
        );
        let v = Polynomial::decompress(&v_decoded, DV, &ring, 1).remove(0);

        let s_ntt = Polynomial::decode13(&ring, &secret_key, self.k as usize);

        // Generate Message
        let mut u_ntt = ring.ntt(&u);
        Self::to_bit_reversed_order_polyvec(&mut u_ntt);

        let mut s_mul_u = self.vector_vector_mul(&s_ntt, &u_ntt);

        // Remove bit-reversed order
        Self::to_bit_reversed_order_poly(&mut s_mul_u);
        let su = ring.inv_ntt(&[s_mul_u]).remove(0);
        let vsu_compressed = Polynomial::compress(&[su - v], 1, 1);
        Polynomial::encode1(&vsu_compressed)
    }

    /// Generate matrix A
    fn gen_matrix_a(&self, ring: &Ring, public_seed: &[u8], transpose: bool) -> Vec<Polynomial> {
        let mut a: Vec<Polynomial> =
            vec![Polynomial::new_unitialized(&ring); (self.k * self.k) as usize];
        let mut shake = sha3::Keccak::shake128();

        a.iter_mut().enumerate().for_each(|(i, current_a)| {
            let mut rand_bytes = [0u8; 768];

            let mut tmp_vec = public_seed.to_vec();
            if !transpose {
                tmp_vec.push((i % self.k as usize) as u8);
                tmp_vec.push((i / self.k as usize) as u8);
            } else {
                tmp_vec.push((i / self.k as usize) as u8);
                tmp_vec.push((i % self.k as usize) as u8);
            }

            shake.absorb(&tmp_vec);

            //
            // Not sure if this coefficient generation is correct.
            //
            // Do first pass to generate the polynomial. There is a possibility that we don't
            // manage to generate enough coefficients.
            let mut poly_final = Polynomial::new_unitialized(&ring);
            shake.squeeze(&mut rand_bytes);
            let poly_gen = ring.gen_coefficients(&rand_bytes);
            for coeff in poly_gen {
                poly_final.push(coeff);
            }

            // If first pass doesn't return 256 coefficients for the polynomial repeat the
            // procedure until it we meet the requirements.
            while poly_final.len() < self.n as usize {
                shake.squeeze(&mut rand_bytes);

                let poly_gen = ring.gen_coefficients(&rand_bytes);
                let poly_final_len = poly_final.len();
                let poly_gen_len = poly_gen.len();

                if poly_final.len() + poly_gen.len() >= self.n as usize {
                    for coeff in poly_gen
                        .iter()
                        .take(self.n as usize - (poly_final_len + poly_gen_len))
                    {
                        poly_final.push(*coeff);
                    }
                } else {
                    for coeff in poly_gen.iter() {
                        poly_final.push(*coeff);
                    }
                }
            }

            *current_a = poly_final;
        });

        a
    }

    fn random_samples_from_ring(
        &self,
        seed: &[u8],
        ring: &Ring,
        nonce_init: usize,
    ) -> (Vec<Polynomial>, Vec<Polynomial>) {
        let mut s: Vec<Polynomial> = vec![Polynomial::new(&ring); self.k as usize];
        let mut e: Vec<Polynomial> = vec![Polynomial::new(&ring); self.k as usize];
        let mut shake = sha3::Keccak::shake256();

        s.iter_mut()
            .zip(e.iter_mut())
            .enumerate()
            .for_each(|(nonce, (current_s, current_e))| {
                let mut bytes_to_gen_coeffs_from = [0u8; 768];
                let nonce = nonce + nonce_init;

                // Generate s
                let mut tmp_vec = seed.to_vec();
                tmp_vec.push(nonce as u8);

                shake.absorb(&tmp_vec);
                shake.squeeze(&mut bytes_to_gen_coeffs_from);

                // Can easily panic
                let coeffs_final = ring.cbd(&bytes_to_gen_coeffs_from);
                *current_s = coeffs_final;

                // Generate e
                let mut tmp_vec = seed.to_vec();
                tmp_vec.push((nonce + self.k as usize) as u8);

                shake.absorb(&tmp_vec);
                shake.squeeze(&mut bytes_to_gen_coeffs_from);

                // Can easily panic
                let coeffs_final = ring.cbd(&bytes_to_gen_coeffs_from);
                *current_e = coeffs_final;
            });

        (s, e)
    }

    pub fn encode_bytes(&self, bytes_to_encode: &[u64]) -> Vec<u8> {
        let mut out: Vec<u8> = vec![];

        bytes_to_encode.iter().for_each(|coeffs| {
            out.push((coeffs & 0x01) as u8);
            out.push(((coeffs & 0x02) >> 1) as u8);
            out.push(((coeffs & 0x04) >> 2) as u8);
            out.push(((coeffs & 0x08) >> 3) as u8);
            out.push(((coeffs & 0x10) >> 4) as u8);
            out.push(((coeffs & 0x20) >> 5) as u8);
            out.push(((coeffs & 0x40) >> 6) as u8);
            out.push(((coeffs & 0x80) >> 7) as u8);
        });

        out
    }

    fn to_bit_reversed_order_polyvec(polynomials: &mut [Polynomial]) {
        for mut poly in polynomials {
            Self::to_bit_reversed_order_poly(&mut poly);
        }
    }

    fn to_bit_reversed_order_poly(poly: &mut Polynomial) {
        let mut coeffs_to_swap = vec![];
        for i in 0..256 {
            if (Self::reverse_bits(i as u8) as usize) >= i {
                coeffs_to_swap.push(i);
            }
        }

        for i in &coeffs_to_swap {
            poly.swap(*i, Self::reverse_bits(*i as u8) as usize);
        }
    }

    fn reverse_bits(mut b: u8) -> u8 {
        b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
        b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
        b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
        b
    }

    fn matrix_vector_mul(&self, matrix: &[Polynomial], vector: &[Polynomial]) -> Vec<Polynomial> {
        let mut out = vec![
            Polynomial::from_parts(self.q as u64, self.n as usize);
            matrix.len() / self.k as usize
        ];

        for i in 0..(self.k as usize) {
            for j in 0..(self.n as usize) {
                let mut sum = Modulo::new(self.q, 0);
                for k in 0..(self.k as usize) {
                    sum += matrix[i * self.k as usize + k][j] * vector[k][j];
                }
                out[i][j] = sum;
            }
        }

        out
    }

    fn vector_vector_mul(&self, vector1: &[Polynomial], vector2: &[Polynomial]) -> Polynomial {
        let mut out = Polynomial::from_parts(self.q, self.n as usize);

        for j in 0..(self.n as usize) {
            let mut sum = Modulo::new(self.q, 0);
            for k in 0..(self.k as usize) {
                sum += vector1[k][j] * vector2[k][j];
            }
            out[j] = sum;
        }

        out
    }

    fn add_vectors(&self, a: Vec<Polynomial>, b: Vec<Polynomial>) -> Vec<Polynomial> {
        let mut out = vec![Polynomial::from_parts(self.q, self.n as usize); self.k as usize];

        out.iter_mut()
            .zip(a.into_iter())
            .zip(b.into_iter())
            .for_each(|((out_poly, poly_a), poly_b)| *out_poly = poly_a + poly_b);

        out
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encode11() {
        let vec: [u64; 8] = [0x400, 0x02a, 0x1ff, 0x285, 0x006, 0x0ea, 0x040, 0x007];
        let kyber = Kyber::v512();

        assert_eq!(
            kyber.encode11(&vec),
            [0x00, 0x54, 0xc1, 0x7f, 0x0a, 0x65, 0x00, 0x75, 0x00, 0xe1, 0x00]
        );
    }

    #[test]
    fn test_decode11() {
        let vec: [u64; 8] = [0x400, 0x02a, 0x1ff, 0x285, 0x006, 0x0ea, 0x040, 0x007];
        let kyber = Kyber::v512();

        assert_eq!(kyber.decode11(&kyber.encode11(&vec)), vec);
    }

    #[test]
    fn test_encode3() {
        let vec: [u64; 8] = [0x0, 0x2, 0x3, 0x5, 0x6, 0x4, 0x1, 0x7];
        let kyber = Kyber::v512();

        assert_eq!(kyber.encode3(&vec), [0xd0, 0x6a, 0xe6]);
    }

    #[test]
    fn test_decode3() {
        let vec = vec![0x0, 0x2, 0x3, 0x5, 0x6, 0x4, 0x1, 0x7];
        let kyber = Kyber::v512();
        let vec_encoded = kyber.encode3(&vec);

        assert_eq!(
            kyber.decode3(&vec_encoded),
            [0x0, 0x2, 0x3, 0x5, 0x6, 0x4, 0x1, 0x7]
        );
    }
}
